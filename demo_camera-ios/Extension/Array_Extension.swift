//
//  Array_Extension.swift
//  
//
//  Created by Admin on 2/12/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
    
}

extension Dictionary where Key == String, Value == String {
    
    mutating func append(anotherDict:[String:String]) {
        for (key, value) in anotherDict {
            self.updateValue(value, forKey: key)
        }
    }
}
