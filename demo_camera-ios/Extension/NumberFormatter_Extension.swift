//
//  NumberFormatter_Extension.swift
//  pricex-ios
//
//  Created by Admin on 20/11/2563 BE.
//  Copyright © 2563 BE Feyverly. All rights reserved.
//

import Foundation

extension NumberFormatter {
    
    static func numberToStringWithComma(value: Int) -> String {
        return numberToStringWithFormat(format: "#,###", num: NSNumber(integerLiteral: value))
    }
    
    static func numberToStringWithComma(value: Double) -> String {
        return numberToStringWithFormat(format: "#,###.##", num: NSNumber(value: value))
    }
    
    static func numberToStringWithFormat(format: String, num: NSNumber) -> String {
        let numFormat = NumberFormatter()
        numFormat.positiveFormat = format
        return unwrapped(numFormat.string(from: num), with: "")
    }
    
    static func stringWithCommaToNumber(numTxt: String) -> NSNumber? {
        return numberFrom(format: "#,###", numTxt: numTxt)
    }
    
    static private func numberFrom(format: String, numTxt: String) -> NSNumber? {
        let numFormat = NumberFormatter()
        numFormat.positiveFormat = format
        return numFormat.number(from: numTxt)
    }
}

extension Int {
    
    func stringWithComma() -> String {
        return NumberFormatter.numberToStringWithComma(value: self)
    }
    
}

extension Double {
    
    func stringWithComma() -> String {
        return NumberFormatter.numberToStringWithComma(value: self)
    }
    
    func stringWithComma2Decimal() -> String {
        return stringWith(format: "#,##0.00")
    }
    
    func stringWithComma1Decimal() -> String {
        return stringWith(format: "#,##0.0")
    }
    
    func stringWith(format: String) -> String {
        return NumberFormatter.numberToStringWithFormat(format: format, num: NSNumber(value: self))
    }
}

