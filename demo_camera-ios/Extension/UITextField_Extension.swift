//
//  UITextField_Extension.swift
//  
//
//  Created by Feyverly on 8/5/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import UIKit

private var maxLengths = [UITextField: Int]()

extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func addToolBarDone(target: Any? = nil, actionDone: Selector? = nil) -> Void {
        let toolBar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 44))
        toolBar.barStyle = UIBarStyle.default
        toolBar.backgroundColor = .white
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) // ใช้เป็น space ตรงกลางเพื่อผลักให้ปุ่ม Done ไปชิดด้านขวา
        
        var doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self,
                                         action: actionDone == nil ? #selector(dismissKeyboard) : actionDone)
        
        let doneTxt = "Done"//LanguageController.share.getTextFromTag(tag: 200004)
        if(doneTxt != ""){
            doneButton = UIBarButtonItem(title: doneTxt, style: .done,
                                         target: actionDone == nil ? self : target,
                                         action: actionDone == nil ? #selector(dismissKeyboard) : actionDone)
            doneButton.setTitleTextAttributes([
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16),
                NSAttributedString.Key.foregroundColor : UIColor.AppColor.charcoal,
                ], for: .normal)
        }
        
        toolBar.items = [flexibleSpace, doneButton]
        self.inputAccessoryView = toolBar
    }
    
    func addToolBarBackNextDone(target: Any?, actionBack: Selector?, actionNext: Selector?,
                                actionDone: Selector? = nil) -> Void {
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 44))
        toolbar.barStyle = UIBarStyle.default
        var doneTxt = "Done"//LanguageController.share.getTextFromTag(tag: 200004)
        if(doneTxt == ""){
            doneTxt = "Done"
        }
        toolbar.items = [
            UIBarButtonItem(title: " < ", style: UIBarButtonItem.Style.plain, target: target, action: actionBack),
            UIBarButtonItem(title: " > ", style: UIBarButtonItem.Style.plain, target: target, action: actionNext),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: doneTxt, style: UIBarButtonItem.Style.plain,
                            target: actionDone == nil ? self : target,
                            action: actionDone == nil ? #selector(dismissKeyboard) : actionDone)]
        toolbar.sizeToFit()
        self.inputAccessoryView = toolbar
    }
    
    @objc func dismissKeyboard() -> Void {
        self.resignFirstResponder()
    }
    
    @IBInspectable var maxLength: Int {
        get {
            guard let length = maxLengths[self] else {
                return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            addTarget(self, action: #selector(limitLength), for: .editingChanged)
        }
    }
    
    @objc func limitLength(textField: UITextField) {
        
        guard let prospectiveText = textField.text, prospectiveText.count > maxLength else {
            return
        }
        
        let selection = selectedTextRange
        
        text = prospectiveText[0..<maxLength]
        let newPosition = textField.position(from: selection!.start, offset: -1)
        if(newPosition != nil){
            selectedTextRange = textRange(from: newPosition!, to: newPosition!)
        }
    }
}
