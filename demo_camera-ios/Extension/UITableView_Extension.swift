//
//  UITableView_Extension.swift
//  
//
//  Created by Feyverly on 25/5/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension UITableView {
    
    func showLoadMore(type: NVActivityIndicatorType = .ballBeat, color: UIColor = .gray, width: CGFloat? = nil) {
        let w = width ?? self.frame.width
        let indicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: w, height: 40.0)
            , type: .ballBeat, color: color, padding: 35.0)
        indicatorView.backgroundColor = .clear
        indicatorView.startAnimating()
        self.tableFooterView = indicatorView
    }
    
    func hideLoadMore(height: CGFloat = 40) {
        DispatchQueue.main.async {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: height))
            view.backgroundColor = .clear
            self.tableFooterView = view
        }
    }
    
    func setTableHeaderView(_ headerView: UIView) {
        headerView.translatesAutoresizingMaskIntoConstraints = false
        self.tableHeaderView = headerView
        headerView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    }
    
    func reloadHeightHeaderTable() -> Void {
        guard let headerView = self.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            self.tableHeaderView = headerView
            self.layoutIfNeeded()
        }
    }
}

extension UITableViewCell {
    var tableView:UITableView?{
        return superview as? UITableView
    }
    
    var indexPath:IndexPath?{
        return tableView?.indexPath(for: self)
    }
}
