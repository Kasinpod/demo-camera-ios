//
//  UIViewController_Extension.swift
//  
//
//  Created by Feyverly on 9/5/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension UIViewController {
    
    func setHideKeyboardWhenTap(view: UIView? = nil) -> Void {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        if view == nil {
            self.view.addGestureRecognizer(gesture)
        }
        else{
            view!.addGestureRecognizer(gesture)
        }
    }
    
    @objc private func hideKeyboard() -> Void {
        self.view.endEditing(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.hideKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func showIndicator(_ type: NVActivityIndicatorType = .ballSpinFadeLoader, message: String? = nil) {
        let data = ActivityData(size: CGSize(width: 40, height: 40), message: message, messageFont: UIFont.systemFont(ofSize: 16), messageSpacing: 10, type: type, color: UIColor.AppColor.outerSpace, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor.white.withAlphaComponent(0.6), textColor: .black)
        
        NVActivityIndicatorPresenter.sharedInstance.setMessage(message)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(data, nil)
    }
    
    func hideIndicator(onComplete:(() -> Void)? = nil) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating { (view, hide) in
            hide()
            onComplete?()
        }
    }
    
    func showDialog(msg: String, onOK: (() -> Void)? = nil) -> Void {
        DispatchQueue.main.async {
            DialogController.share.showOneBtnDialog(vc: self, title: msg, message: "", onOK: onOK)
        }
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: false)
                return
            }
        }
    }
    
}

extension UINavigationController {

   func popBack<T: UIViewController>(toControllerType: T.Type) {
       var viewControllers: [UIViewController] = self.viewControllers
       viewControllers = viewControllers.reversed()
       for currentViewController in viewControllers {
           if currentViewController .isKind(of: toControllerType) {
               self.popToViewController(currentViewController, animated: false)
               break
           }
       }
   }

}
