//
//  URLRequest_Extension.swift
//  hi-powershot-ios
//
//  Created by Admin on 29/10/2563 BE.
//

import Foundation

extension URLRequest {

    var httpBodyDictionary: [String : String] {
        var dictionary : [String : String] = [:]
        guard let httpBody = self.httpBody, let parameterString = String(data: httpBody, encoding: .utf8) else {
            return dictionary
        }

        parameterString.components(separatedBy: "&").forEach {
            let componants = $0.components(separatedBy: "=")
            if componants.count < 2 {
                return
            }
            guard let name = componants[0].removingPercentEncoding, let value = componants[1].removingPercentEncoding else {
                return
            }
            dictionary[name] = value
        }
        return dictionary
    }
}
