//
//  UIColor_Extension.swift
//  
//
//  Created by Feyverly on 8/5/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex:Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    struct AppColor {
        static let charcoal = UIColor(red: 43, green: 58, blue: 79)
        static let outerSpace = UIColor(red: 72, green: 72, blue: 72)
        static let whiteSmoke = UIColor(red: 246, green: 246, blue: 246)
        static let Isabelline = UIColor(red: 236, green: 236, blue: 238)
        static let darkJungleGreen = UIColor(red: 30, green: 32, blue: 42)
        static let timberwolf = UIColor(red: 216, green: 216, blue: 216)
        static let munsell = UIColor(red: 241, green: 243, blue: 246)
        static let azure = UIColor(red: 0, green: 122, blue: 255)
        static let manatee = UIColor(red: 156, green: 156, blue: 156)
        static let gold = UIColor(red: 255, green: 176, blue: 0)
        static let lust = UIColor(red: 255, green: 31, blue: 31)
        static let green = UIColor(red: 0, green: 213, blue: 118)
        static let gainsboro = UIColor(red: 217, green: 217, blue: 217)
        static let lightGreen = UIColor(red: 178, green: 199, blue: 94)
    }
    
    func brightened(by factor: CGFloat) -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: b * factor, alpha: a)
    }
    
    func image(_ size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}
