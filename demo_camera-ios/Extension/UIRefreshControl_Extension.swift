//
//  UIRefreshControl_Extension.swift
//  
//
//  Created by Feyverly on 31/5/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import UIKit

extension UIRefreshControl {
    func beginRefreshing(in tableView: UITableView) {
        beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: -(frame.size.height+5))
        tableView.setContentOffset(offsetPoint, animated: true)
    }
}
