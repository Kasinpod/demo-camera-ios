//
//  UIScrollView_Extension.swift
//  
//
//  Created by Feyverly on 25/7/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    func scrollToTop(animate: Bool) {
        setContentOffset(CGPoint.zero, animated: animate)
    }
    
    func scrollToBottom(animate: Bool) {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        setContentOffset(bottomOffset, animated: animate)
    }
    
    func scrollToLeft(animate: Bool) -> Void {
        setContentOffset(CGPoint.zero, animated: animate)
    }
    
    func scrollToRight(animate: Bool) -> Void {
        let rightOffset = CGPoint(x: contentSize.width - bounds.size.width + contentInset.right, y: 0)
        setContentOffset(rightOffset, animated: animate)
    }
}
