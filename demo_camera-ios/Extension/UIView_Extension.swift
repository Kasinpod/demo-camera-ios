//
//  UIView_Extension.swift
//
//
//  Created by Feyverly on 8/5/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import UIKit

extension UIView {
    func roundView() {
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        
    }
    
    func roundCorners(_ cormerMask:CACornerMask, radius: CGFloat) {
        self.clipsToBounds = false
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = cormerMask
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func roundCornerView(rad:CGFloat) {
        self.layer.cornerRadius = rad
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func clearRoundView() -> Void {
        self.roundCorners([.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner], radius: 0)
    }
    
    func addShadow(color: UIColor = .black) {
        self.addShadow(color: color,
                       shadowOffset: CGSize(width: 1, height: 1),
                       shadowOpacity: 0.2,
                       shadowRadius: 1.0)
    }
    
    func addShadow(color: UIColor = .black, shadowOffset: CGSize, shadowOpacity: Float, shadowRadius: CGFloat) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
        self.clipsToBounds = false
    }
    
    func clearShadow() {
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        self.clipsToBounds = false
    }
    
    func addBorder(color:UIColor, width:CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func clearBorder() {
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 0
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, lenghtDash: Int, lengthGap: Int,
                        lineWidth: CGFloat, color: UIColor) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineDashPattern = [NSNumber(integerLiteral: lenghtDash), NSNumber(integerLiteral: lengthGap)]
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
    func clearDottedLine() -> Void {
        if let index = self.layer.sublayers?.firstIndex(where: {$0 is CAShapeLayer}) {
            self.layer.sublayers?.remove(at: index)
        }
        
    }
    
    func addBlurEffect(style: UIBlurEffect.Style) -> Void {
        self.layoutIfNeeded()
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func radians(degrees: CGFloat) -> CGFloat {
        return (degrees * CGFloat.pi) / 180.0
    }
    
    func startWobbling () {
        guard self.transform == CGAffineTransform.identity else { return }  // Stop a duplicated animation
        let r = CGFloat(arc4random_uniform(1)) - 0.5
        let rotateDegree: CGFloat = 2.0
        let leftWobble = CGAffineTransform.identity.rotated(by: self.radians(degrees: rotateDegree * -1 - r))
        let rightWobble = CGAffineTransform.identity.rotated(by: self.radians(degrees: rotateDegree + r))
        self.transform = leftWobble
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: [UIView.AnimationOptions.allowUserInteraction, UIView.AnimationOptions.repeat, UIView.AnimationOptions.autoreverse],
                       animations: {
                        self.transform = rightWobble
                       }, completion: { _ in
                       })
    }
    
    func stopWobbling () {
        self.layer.removeAllAnimations()
        self.transform = CGAffineTransform.identity
    }
    
    func rotate(angle: CGFloat, animate: Bool) {
        let radians = angle / 180.0 * CGFloat.pi
        UIView.animate(withDuration: animate ? 0.25 : 0, animations: {
            self.transform = self.transform.rotated(by: radians)
        })
    }
    
    func clearTransform(animate: Bool) -> Void {
        UIView.animate(withDuration: animate ? 0.25 : 0, animations: {
            self.transform = .identity
        })
    }
    
    func setIsHidden(_ hidden: Bool, animated: Bool) {
        if animated {
            if self.isHidden && !hidden {
                self.alpha = 0.0
                self.isHidden = false
            }
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha = hidden ? 0.0 : 1.0
            }) { (complete) in
                self.isHidden = hidden
            }
        } else {
            self.isHidden = hidden
        }
    }
    
    func hapticFeedback(style: UIImpactFeedbackGenerator.FeedbackStyle) {
        let feedbackGenerator = UIImpactFeedbackGenerator(style: style)
        feedbackGenerator.impactOccurred()
    }
    
    @IBInspectable var circleShape : Bool {
        get {
            return false
        }
        set {
            self.layer.cornerRadius = round((self.bounds.size.height)/2.0)
            self.layer.masksToBounds = true
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable var shadow: Bool {
        get {
            return false
        }
        set {
            if newValue {
                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
                layer.shadowRadius = 4.0
                layer.shadowOpacity = 0.5
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
            layer.masksToBounds = true
            clipsToBounds = true
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    func fadeIn(_ duration: TimeInterval? = 0.2, onCompletion: (() -> Void)? = nil) {
        self.alpha = 0
        self.isHidden = false
        UIView.animate(withDuration: duration!,
                       animations: { self.alpha = 1 },
                       completion: { (value: Bool) in
                        if let complete = onCompletion { complete() }
                       }
        )
    }
    
    func fadeOut(_ duration: TimeInterval? = 0.2, onCompletion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration!,
                       animations: { self.alpha = 0 },
                       completion: { (value: Bool) in
                        self.isHidden = true
                        if let complete = onCompletion { complete() }
                       }
        )
    }
    
    func makeSnapshot() -> UIImage? {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: frame.size)
            return renderer.image { _ in drawHierarchy(in: bounds, afterScreenUpdates: true) }
        } else {
            return layer.makeSnapshot()
        }
    }
}


typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        return points.startPoint
    }
    
    var endPoint : CGPoint {
        return points.endPoint
    }
    
    var points : GradientPoints {
        switch self {
        case .topRightBottomLeft:
            return (CGPoint(x: 0.0,y: 1.0), CGPoint(x: 1.0,y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 1,y: 1))
        case .horizontal:
            return (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5))
        case .vertical:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
        }
    }
}

extension UIView {
    
    func applyGradient(with colours: [UIColor], locations: [NSNumber]? = nil) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(with colours: [UIColor], gradient orientation: GradientOrientation, cornerRadius: CGFloat) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.cornerRadius = cornerRadius
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func clearGradient() {
        if let gradient = self.layer.sublayers?.first as? CAGradientLayer {
            gradient.removeFromSuperlayer()
        }
    }
}

extension UIView {
    
    func addTapGesture(action : @escaping ()->Void ){
        let tap = MyTapGestureRecognizer(target: self , action: #selector(self.handleTap(_:)))
        tap.action = action
        tap.numberOfTapsRequired = 1
        
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
        
    }
    
    @objc func handleTap(_ sender: MyTapGestureRecognizer) {
        sender.action!()
    }
}

class MyTapGestureRecognizer: UITapGestureRecognizer {
    var action : (()->Void)? = nil
}
