//
//  Array_String_Extension.swift
//  
//
//  Created by Admin on 19/2/2563 BE.
//  Copyright © 2563 MixsUp. All rights reserved.
//

import SwiftyJSON

extension Array where Element == String {
    
    static func from(jsonArray: [JSON]) -> [Element] {
        return jsonArray.map({ $0.stringValue })
    }
    
    func toJSONArray() -> [JSON] {
        return self.map({ JSON($0) })
    }

}
