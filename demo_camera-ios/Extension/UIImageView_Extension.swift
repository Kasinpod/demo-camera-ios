//
//  UIImageView_Extension.swift
//  
//
//  Created by Feyverly on 14/6/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func setImage(with: URL?) -> Void {
        self.kf.setImage(with: with)
    }
    
}

