//
//  UIFont_Extension.swift
//  
//
//  Created by Feyverly on 22/5/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import UIKit

extension UIFont {
    
//    static func sukhumvitFont(style: FontStyle, size: CGFloat = 16) -> UIFont {
//        var fontName = "SukhumvitSet"
//        fontName = "\(fontName)-\(style.rawValue)"
//        return UIFont(name: fontName, size: size)!
//    }
    
}

enum FontStyle : String {
    case medium = "Medium"
    case semiBold = "SemiBold"
    case regular = "Regular"
    case bold = "Bold"
    case thin = "Thin"
    case text = "Text"
    case light = "Light"
}
