//
//  SwiftyJSON_Extension.swift
//  
//
//  Created by Admin on 11/2/2563 BE.
//  Copyright © 2563 MixsUp. All rights reserved.
//

import SwiftyJSON

extension JSON {
    
    func toDate(format: String) -> Date? {
        if type != .string {
            return nil
        }
        
        return DateFormatter.dateFrom(string: stringValue, format: format)
    }
    
    func toDateFromTimeInterval1970() -> Date? {
        guard let double = double else { return nil }
        return Date(timeIntervalSince1970: double)
    }
    
    func toDateFromTimeInterval1970NonNil() -> Date {
        return Date(timeIntervalSince1970: doubleValue)
    }
    
}
