//
//  URL_Extension.swift
//  
//
//  Created by Admin on 16/12/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import Alamofire

extension URL {
    func appendParameters(params: Parameters) -> URL? {
        var components = URLComponents(string: self.absoluteString)
        
        if components?.queryItems == nil || components?.queryItems?.count == 0 {
            components?.queryItems = params.map { element in URLQueryItem(name: element.key, value: element.value as? String) }
        }
        else if let queryItems = components?.queryItems {
            
            params.forEach { (key, value) in
                if let index = queryItems.firstIndex(where: { $0.name == key }) {
                    components?.queryItems?[index].value = value as? String
                }
                else {
                    components?.queryItems?.append(URLQueryItem(name: key, value: value as? String))
                }
            }
        }
        
        return components?.url
    }
    
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}
