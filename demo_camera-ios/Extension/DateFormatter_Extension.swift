//
//  DateFormatter_Extension.swift
//  
//
//  Created by Feyverly on 14/8/2561 BE.
//  Copyright © 2561 Feyverly. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static func dateFrom(string: String, format: String) -> Date? {
        let dFormat = DateFormatter()
        let identifier = "en_us"
        dFormat.locale = Locale(identifier: identifier)
        dFormat.dateFormat = format
        return dFormat.date(from: string)
    }
    
}
