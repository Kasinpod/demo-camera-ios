//
//  Global.swift
//  
//
//  Created by Admin on 22/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import Foundation
import SwiftyJSON

public func unwrapped<T>(_ wrapped: T?, with castValue: T) -> T {
    if let unwrapped = wrapped {
        return unwrapped
    }
    else{
        return castValue
    }
}

public func ternary<T>(condition: Bool, true t: T, false f: T) -> T {
    if condition {
        return t
    } else {
        return f
    }
}

public func secondsToHoursMinutesSeconds (seconds : Double) -> (Double, Double, Double) {
    let (hr,  minf) = modf (seconds / 3600)
    let (min, secf) = modf (60 * minf)
    return (hr, min, 60 * secf)
}

public func secondsToMinutesSeconds (seconds : Int) -> (Int, Int) {
    return (seconds / 60, seconds % 60)
}

public func jsonToData(json: JSON) -> Data? {
    do {
        return try json.rawData(options: .prettyPrinted)
    } catch _ {
        return nil
    }
}
