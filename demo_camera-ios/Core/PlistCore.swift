//
//  PlistCore.swift
//  
//
//  Created by Admin on 22/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import Foundation

enum PlistFilename: String {
    case uiText = "UIText"
}

final class PlistCore {
    
    static let share = PlistCore()
    private let fileManager:FileManager!
    
    private init() {
        fileManager = FileManager.default
    }
    
    func readPlistArrayFromFileName(filename: PlistFilename) -> [Any]? {
        let path = filePathForFilename(filename: filename)
        let dict = NSDictionary(contentsOfFile: path)
        let data = dict?["data"]
        return data as? [Any]
    }
    
    func readPlistDictFromFileName(filename: PlistFilename) -> [String:Any]? {
        let path = filePathForFilename(filename: filename)
        do {
            let rawData = try Data(contentsOf: URL(fileURLWithPath: path))
            let realData = try PropertyListSerialization.propertyList(from: rawData, format: nil) as? [String:Any]
            return realData
        }
        catch {
            return nil
        }
        
    }
    
    func writePlistDict(data:[String:Any], filename: PlistFilename) -> Bool {
        let path = filePathForFilename(filename: filename)
        #if DEBUG
        print("path : \(path)\n")
        #endif
        if (!(fileManager.fileExists(atPath: path)))
        {
            do {
                let bundle:String = Bundle.main.path(forResource: filename.rawValue, ofType: "plist")!
                try fileManager.copyItem(atPath: bundle, toPath: path)
            } catch {
                print("Fail to open \(filename).plist")
            }
        }
        
        let tmp:NSDictionary = NSDictionary(dictionary: data)
        tmp.write(toFile: path, atomically: true)
        
        return true
    }
    
    func writePlistDict(data:[Any], filename: PlistFilename) -> Bool {
        let path = filePathForFilename(filename: filename)
        
        if (!(fileManager.fileExists(atPath: path)))
        {
            do {
                let bundle:String = Bundle.main.path(forResource: filename.rawValue, ofType: "plist")!
                try fileManager.copyItem(atPath: bundle, toPath: path)
            } catch {
                print("Fail to open \(filename).plist")
            }
        }
        
        let tmp:NSArray = NSArray(array: data)
        let dict = NSMutableDictionary()
        dict["data"] = tmp
        dict.write(toFile: path, atomically: true)
        
        return true
    }
    
    func removePlistDict(filename: PlistFilename) -> Bool {
        return self.writePlistDict(data: ["":""], filename: filename)
    }
    
    private func filePathForFilename(filename: PlistFilename) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let fullfilename = filename.rawValue + ".plist"
        let path = paths.appendingPathComponent(fullfilename)
        return path
    }
    
    func readFileFromBundleWithFileName(filename: PlistFilename) -> [String:Any]? {
        if let path = Bundle.main.path(forResource: filename.rawValue, ofType: "plist") {
            if let dic = NSDictionary(contentsOfFile: path) as? [String: Any] {
                return dic
            }
        }
        return nil
    }
    
}

