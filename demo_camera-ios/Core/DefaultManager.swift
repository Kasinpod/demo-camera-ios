//
//  DefaultManager.swift
//  
//
//  Created by Admin on 21/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import Foundation

final class DefaultManager {
    
    static let share = DefaultManager()
    
    private init() {
    }
    
    required init?(coder aDecoder: NSCoder) {
    }
    
    func setBool(value: Bool, key: UserDefaultKey) -> Void {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key.rawValue)
    }
    
    func getBool(key: UserDefaultKey) -> Bool {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: key.rawValue)
    }
    
    func setInt(value: Int, key: UserDefaultKey) -> Void {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key.rawValue)
    }
    
    func getInt(key: UserDefaultKey) -> Int {
        let defaults = UserDefaults.standard
        return defaults.integer(forKey: key.rawValue)
    }
    
    func setString(value: String, key: UserDefaultKey) -> Void {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key.rawValue)
    }
    
    func getString(key: UserDefaultKey) -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: key.rawValue)
    }
    
    func setDictionary(dict: [String: Any], key: UserDefaultKey) -> Void {
        let defaults = UserDefaults.standard
        defaults.set(dict, forKey: key.rawValue)
    }
    
    func getDictionary(key: UserDefaultKey) -> [String: Any]? {
        let defaults = UserDefaults.standard
        return defaults.dictionary(forKey: key.rawValue)
    }
    
    func remove(key: UserDefaultKey) -> Void {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key.rawValue)
    }
}

