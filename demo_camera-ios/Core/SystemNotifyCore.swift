//
//  SystemNotifyCore.swift
//  
//
//  Created by Admin on 22/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import Foundation

final class SystemNotifyCore {
    
    static let share = SystemNotifyCore()
    
    private init() {
    }
    
    required init?(coder aDecoder: NSCoder) {
    }
    
    func addObserver(observer: Any, selector: Selector, appEvent: AppEventProtocol, object: Any?) -> Void {
        NotificationCenter.default.addObserver(
            observer, selector: selector,
            name: NSNotification.Name(appEvent.name),
            object: object)
    }
    
    func post(appEvent: AppEventProtocol, object: Any? = nil, userInfo: [AnyHashable : Any]? = nil) -> Void {
        NotificationCenter.default.post(name: Notification.Name(appEvent.name), object: object, userInfo: userInfo)
    }
    
}
