//
//  LocationCore.swift
//  pricex-ios
//
//  Created by Admin on 30/11/2563 BE.
//  Copyright © 2563 BE Feyverly. All rights reserved.
//

import CoreLocation

final class LocationCore: NSObject, CLLocationManagerDelegate {
    
    static let share = LocationCore()
    
    fileprivate var locManager : CLLocationManager?
    private var onCompletion: ((CLLocation) -> ())?
    private var onError: (() -> Void)?
    
    var status: CLAuthorizationStatus = .notDetermined
    var coordinate = CLLocationCoordinate2D.init(latitude: 0, longitude: 0)
    
    private override init() {
        super.init()
        locManager = CLLocationManager()
        locManager?.delegate = self
        locManager?.desiredAccuracy = kCLLocationAccuracyBest
        let authorizationStatus = CLLocationManager.authorizationStatus()
        status = authorizationStatus
        if (authorizationStatus == CLAuthorizationStatus.notDetermined) {
            locManager?.requestWhenInUseAuthorization()
        }
        else {
            if CLLocationManager.locationServicesEnabled() {
                locManager?.startUpdatingLocation()
            }
            if (authorizationStatus == CLAuthorizationStatus.authorizedWhenInUse ||
                authorizationStatus == CLAuthorizationStatus.authorizedAlways){
                if let coordinate = locManager?.location?.coordinate {
                    self.coordinate = coordinate
                }
            }
        }
    }
    
    func getCurrentLocation(onCompletion: @escaping((CLLocation) -> ()),
                            onError: @escaping (() -> Void)) -> Void {
        self.onCompletion = onCompletion
        self.onError = onError
        locManager?.delegate = self
        locManager?.desiredAccuracy = kCLLocationAccuracyBest
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if (authorizationStatus == CLAuthorizationStatus.notDetermined) {
            locManager?.requestWhenInUseAuthorization()
            return
        }
        if CLLocationManager.locationServicesEnabled() {
            locManager?.startUpdatingLocation()
        }
        if (authorizationStatus == CLAuthorizationStatus.authorizedWhenInUse ||
            authorizationStatus == CLAuthorizationStatus.authorizedAlways) {
            if let location = locManager?.location {
                onCompletion(location)
            } else {
                onError()
            }
        }
        else {
            onError()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        #if DEBUG || UAT
        print("Error \(error)")
        #endif
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedAlways || status == .authorizedWhenInUse) {
            if let currentLocation = locManager?.location {
                self.coordinate = currentLocation.coordinate
                onCompletion?(currentLocation)
            }
            else {
                onError?()
            }
        }
        else if (status == .denied || status == .restricted)    {
            onError?()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastLoc = locations.last {
            self.coordinate = lastLoc.coordinate
        }
    }
    
    func getAddress(location: CLLocation, onComplete: @escaping ((CLPlacemark)-> Void), onError: (()-> Void)?) {
        CLGeocoder().reverseGeocodeLocation(location, preferredLocale: Locale(identifier: "en_us")) { (placemarks, error) in
            if error != nil {
                onError?()
                return
            }
            
            guard let placemark = placemarks?.first else {
                onError?()
                return
            }

            onComplete(placemark)
        }
    }
}

