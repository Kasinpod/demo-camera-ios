//
//  SystemCore.swift
//
//
//  Created by Admin on 21/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import Foundation
import SwiftyJSON

enum AppLanguage : String {
    case en = "en"
    case th = "th"
    case ch = "ch"
    case jp = "jp"
    case fr = "fr"
    case kr = "kr"
}

final class SystemCore {
    
    static let share = SystemCore()
    
    private(set) var access_token = ""
    var currentVersion = ""
    var currentLanguage = AppLanguage.en
    private(set) var authorization = ""
    private(set) var newUserId = 0
    
    private init() {
        setAccessToken(token: unwrapped(DefaultManager.share.getString(key: .Token), with: ""))
        currentVersion = getCurrentAppVersion()
        setSystemLanguage(lang: getDeviceLanguage())
//        newUserId = getNewUserId()
        newUserId = 10630
    }
    
    required init?(coder aDecoder: NSCoder) {
    }
    
    func getIsSeenTutorial() -> Bool {
        return DefaultManager.share.getBool(key: .IsSeenTutorial)
    }
    
    func setIsSeenTutorial() -> Void {
        DefaultManager.share.setBool(value: true, key: .IsSeenTutorial)
    }
    
    func getFCMToken() -> String {
        return DefaultManager.share.getString(key: .FCMToken) ?? ""
    }
    
    func setFCMToken(fcm: String) -> Void {
        DefaultManager.share.setString(value: fcm, key: .FCMToken)
    }
    
    func getNewUserId() -> Int {
        return DefaultManager.share.getInt(key: .NewUserId)
    }
    
    func setNewUserId(userId: Int) -> Void {
        self.newUserId = userId
        DefaultManager.share.setInt(value: userId, key: .NewUserId)
    }
    
    func setLanguage(to: AppLanguage) -> Void {
        setSystemLanguage(lang: to.rawValue)
    }
    
    func setAccessToken(token: String) {
//        self.access_token = token
        self.access_token = "0ee5a67e971a765d63d2f3406a96aabf6496331a2ca9e12ad320210621112418"
        self.authorization = "Bearer \(access_token)"
        DefaultManager.share.setString(value: token, key: .Token)
    }
    
    private func setSystemLanguage(lang: String) -> Void {
        if !lang.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
//            let l = AppLanguage(rawValue: lang)
            currentLanguage = AppLanguage.en
            DefaultManager.share.setString(value: lang, key: .Language)
        }
    }
    
    private func getDeviceLanguage() -> String {
        if let lang = DefaultManager.share.getString(key: .Language) {
            if(lang != ""){
                return lang
            }
        }
        
        var result = "en"
        let lang = NSLocale.preferredLanguages.first
        
        if(lang != nil) {
            let index = lang!.index(lang!.startIndex, offsetBy: 2)
            if(result.lowercased() == "th"){
                result = String(lang![..<index])
            }
        }
        return result.lowercased()
    }
    
    private func getCurrentAppVersion() -> String {
        return unwrapped((Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String), with: "")
    }
    
}

