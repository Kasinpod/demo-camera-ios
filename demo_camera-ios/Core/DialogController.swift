//
//  DialogController.swift
//  
//
//  Created by Admin on 31/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import UIKit
import SwiftEntryKit

final class DialogController {
    
    static let share = DialogController()
    
    private init() {
    }
    
    required init?(coder aDecoder: NSCoder) {
    }
    
    func showOneBtnDialog(vc: UIViewController,
                          title: String, message: String = "",
                          onOK: (() -> Void)? = nil, titleBtn: String = "OK",
                          canDissmis: Bool = true) -> Void {
        let dialogView = DialogView()
        dialogView.canDissmisOK = canDissmis
        dialogView.setupBtn(title: title, btnOKText: titleBtn, onOK: onOK)
        
        let attributes = centerPopupAlertAttributes
        SwiftEntryKit.display(entry: dialogView, using: attributes, presentInsideKeyWindow: true, rollbackWindow: .main)
    }
    
    func showTwoBtnDialog(vc: UIViewController,
                          title: String, message: String = "",
                          okText: String = "OK", onOK:(() -> Void)? = nil,
                          cancelText: String = "Cancel", onCancel:(() -> Void)? = nil,
                          canDissmisOK: Bool = true,
                          canDissmisCancel: Bool = true) -> Void {
        
        let dialogView = DialogView()
        dialogView.setupBtn(title: title, btnOKText: okText, onOK: onOK, btnCancelText: cancelText, onCancel: onCancel)

        dialogView.canDissmisOK = canDissmisOK
        dialogView.canDissmisCancel = canDissmisCancel
        
        let attributes = centerPopupAlertAttributes
        SwiftEntryKit.display(entry: dialogView, using: attributes, presentInsideKeyWindow: true, rollbackWindow: .main)
    }
    
    var centerPopupAlertAttributes: EKAttributes {
        var attributes = EKAttributes.centerFloat
        attributes = EKAttributes.centerFloat
        attributes.hapticFeedbackType = .none
        attributes.displayDuration = .infinity
        attributes.entryBackground = .clear
        attributes.screenBackground = .color(color: EKColor.black.with(alpha: 0.3))
        attributes.shadow = .active(
            with: .init(
                color: .black,
                opacity: 0.3,
                radius: 8
            )
        )
        attributes.screenInteraction = .absorbTouches
        attributes.entryInteraction = .absorbTouches
        attributes.scroll = .enabled(
            swipeable: true,
            pullbackAnimation: .jolt
        )
        attributes.roundCorners = .all(radius: 8)
        attributes.entranceAnimation = .init(
            scale: .init(
                from: 0.7,
                to: 1,
                duration: 0.4,
                spring: .init(damping: 1, initialVelocity: 0)
            ),
            fade: .init(from: 0, to: 1, duration: 0.4, delay: 0, spring: .init(damping: 0.7, initialVelocity: 0))
        )
        attributes.exitAnimation = .init(
            scale: .init(
                from: 1,
                to: 0.7,
                duration: 0.4,
                spring: .init(damping: 1, initialVelocity: 0)
            ),
            fade: .init(from: 1, to: 0, duration: 0.4, delay: 0, spring: .init(damping: 0.7, initialVelocity: 0))
        )
        attributes.popBehavior = .animated(
            animation: .init(
                translate: .init(duration: 0.35)
            )
        )
        attributes.positionConstraints = .fullWidth
        attributes.statusBar = .light
        return attributes
    }

}

