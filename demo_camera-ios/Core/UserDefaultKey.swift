//
//  UserDefaultKey.swift
//  
//
//  Created by Admin on 21/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import Foundation

enum UserDefaultKey: String {
    case IsSeenTutorial = "IsSeenTutorial"
    case Token = "Token"
    case User = "User"
    case Language = "Language"
    case LanguageVersion = "LanguageVersion"
    case CartBadge = "CartBadge"
    case FCMToken = "FCMToken"
    case isUpdateFCMTokenSuccess = "hasErrorUpdatedFCMToken"
    case AppSetting = "AppSetting"
    case SettingJobs = "SettingJobs"
    case Currency = "Currency"
    case NewUserId = "NewUserId"
}
