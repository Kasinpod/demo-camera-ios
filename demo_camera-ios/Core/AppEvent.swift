//
//  AppEvent.swift
//  
//
//  Created by Admin on 22/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

protocol AppEventProtocol {
    var name: String { get }
}

enum AppEvent {
    
    // MARK: System
    struct ApplicationSoftUpdate : AppEventProtocol {
        typealias Param = AppEvent.ApplicationSoftUpdate.Parameter
        var name = "APPLICATION_SOFT_UPDATE"
        var param : Param
        
        static let share = ApplicationSoftUpdate()

        private init() {
            param = Param()
        }
        
        struct Parameter {
            let version = "version"
        }
    }

    struct ApplicationForceUpdate : AppEventProtocol {
        typealias Param = AppEvent.ApplicationForceUpdate.Parameter
        var name = "APPLICATION_FORCE_UPDATE"
        var param : Param
        
        static let share = ApplicationForceUpdate()
        
        private init() {
            param = Param()
        }
        
        struct Parameter {
            let version = "version"
        }
    }

    struct UserSessionInvalid : AppEventProtocol {
        var name = "USER_SESSION_INVALID"
        
        static let share = UserSessionInvalid()
        
        private init() {
        }
    }
    
    struct UserSessionDeleted : AppEventProtocol {
        var name = "USER_SESSION_DELETED"
        
        static let share = UserSessionDeleted()
        
        private init() {
        }
    }
    
    struct UserSessionRequired : AppEventProtocol {
        var name = "USER_SESSION_REQURIED"
        
        static let share = UserSessionRequired()
        
        private init() {
        }
    }
    
    struct ApplicationServiceNotAvailable : AppEventProtocol {
        var name = "APPLICATION_SERVICE_NOT_AVAILABLE"
        
        static let share = ApplicationServiceNotAvailable()
        
        private init() {
        }
    }
    
    struct ApplicationNeedUpdate : AppEventProtocol {
        var name = "APPLICATION_NEED_UPDATE"
        
        static let share = ApplicationNeedUpdate()
        
        private init() {
        }
    }
    
    struct ApplicationContinueResponse : AppEventProtocol {
        var name = "APPLICATION_CONTINUE_RESPONSE"
        
        static let share = ApplicationContinueResponse()
        
        private init() {
        }
    }
    
    struct DidReciveNotification : AppEventProtocol {
        var name = "DID_RECIVE_NOTIFICATION"
        
        static let share = DidReciveNotification()
        
        private init() {
        }
    }
    
    struct ApplicationUpdateBadge : AppEventProtocol {
        var name = "APPLICATION_UPDATE_BADGE"
        
        static let share = ApplicationUpdateBadge()
        
        private init() {
        }
    }

    // MARK: User
    struct UpdateUser : AppEventProtocol {
        var name = "UPDATE_USER"
        
        static let share = UpdateUser()
        
        private init() {
        }
    }
    
    // MARK: Offer
    struct OfferListUpdate : AppEventProtocol {
        typealias Param = AppEvent.OfferListUpdate.Parameter
        var name = "OFFER_LIST_UPDATE"
        var param : Param
        
        static let share = OfferListUpdate()
        
        private init() {
            param = Param()
        }
        
        struct Parameter {
            let type = "type"
        }
    }
}
