//
//  ManagerCore.swift
//  
//
//  Created by Admin on 22/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import UIKit
import Photos
import AVFoundation

final class ManagerCore {
    
    static let share = ManagerCore()
    
    private let systemCore = SystemCore.share
    private let systemNotifyCore = SystemNotifyCore.share
//    private let locationCore = LocationCore.share
    
    private init() {
        SystemNotifyCore.share.addObserver(observer: self, selector: #selector(showForceUpdate), appEvent: AppEvent.ApplicationForceUpdate.share, object: nil)
        SystemNotifyCore.share.addObserver(observer: self, selector: #selector(showSoftUpdate), appEvent: AppEvent.ApplicationSoftUpdate.share, object: nil)
        SystemNotifyCore.share.addObserver(observer: self, selector: #selector(showUserSessionInvalid), appEvent: AppEvent.UserSessionInvalid.share, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
    }
    
    
    func openGoogleMap(lat: Double, lng: Double) -> Void {
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            let url = URL(string: "comgooglemaps://?center=\(lat),\(lng)&zoom=14&views=traffic&q=\(lat),\(lng)")!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            let url = URL(string: "http://maps.google.com/maps?q=loc:\(lat),\(lng)&zoom=14&views=traffic")!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func openGoogleMapNavigate(fromLat: Double, fromLng: Double,
                               toLat: Double, toLng: Double) -> Void {
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            let url = URL(string: "comgooglemaps://?saddr=\(fromLat),\(fromLng)&daddr=\(toLat),\(toLng)")!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            let url = URL(string: "https://www.google.com/maps/dir/?api=1&origin=\(fromLat),\(fromLng)&destination=\(toLat),\(toLng)")!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    func restartApp() -> Void {
        let rootController = StoryboardScene.Main.initialScene.instantiate()
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController = nil
            window.rootViewController = rootController
        }
    }
    
    @objc func showSoftUpdate(notification: NSNotification) -> Void {
        guard let topVC = UIApplication.topTabbarViewController() else {
            return
        }
        topVC.hideIndicator()
        let obj = notification.object as! [String : Any]?
        let softEvent = AppEvent.ApplicationSoftUpdate.share
        if let version = obj?[softEvent.param.version] as? String {
            let text = "App %@ พร้อมให้บริการแล้ว"
            let title = String(format: text, version)
            DialogController.share.showTwoBtnDialog(vc: topVC, title: title, message: "", okText: "OK", onOK: {
                let url = URL(string: "https://itunes.apple.com/th/app/")
                UIApplication.shared.open(url!, options: [:], completionHandler: { (_) in
                })
            }, cancelText: "Cancel", onCancel: {
                SystemNotifyCore.share.post(appEvent: AppEvent.ApplicationContinueResponse.share)
            }, canDissmisOK: false, canDissmisCancel: true)
        }
    }
    
    @objc func showForceUpdate(notification: NSNotification) -> Void {
        guard let topVC = UIApplication.topTabbarViewController() else {
            return
        }
        topVC.hideIndicator()
        let title = "กรุณาอัพเดทเวอร์ชั่น App"
        DialogController.share.showOneBtnDialog(vc: topVC, title: title, message: "", onOK: {
            let url = URL(string: "https://itunes.apple.com/th/app/")
            UIApplication.shared.open(url!, options: [:], completionHandler: { (_) in
            })
        }, canDissmis: false)
    }
    
    @objc func showUserSessionInvalid(notification: NSNotification) -> Void {
        guard let message = notification.object as? String,
            let topVC = UIApplication.topTabbarViewController() else {
            return
        }
        topVC.hideIndicator()
        DialogController.share.showOneBtnDialog(vc: topVC, title: message, message: "", onOK: {
            SystemCore.share.setAccessToken(token: "")
            ManagerCore.share.restartApp()
        })
    }
    
    func openAppSettings() {
        guard let appSettings = URL(string: UIApplication.openSettingsURLString) else { return }
            UIApplication.shared.open(appSettings, options: [:], completionHandler: { (_) in
        })
    }
    
    func showDialogLocationGoToSetting(onCancel: (()->Void)? = nil)-> Void {
        guard let topVC = UIApplication.topTabbarViewController() else { return }
        let title = "กรุณาอนุญาติการเข้าถึงตำแหน่งของคุณ"
        let goToSettingTxt = "ไปที่ตั้งค่า"
        let cancelText = "ยกเลิก"
        DialogController.share.showTwoBtnDialog(vc: topVC, title: title, message: "", okText: goToSettingTxt, onOK: {
            self.openAppSettings()
        }, cancelText: cancelText, onCancel: onCancel, canDissmisOK: true, canDissmisCancel: true)
    }
    
    func showDialogCameraGoToSetting(onCancel: (()->Void)? = nil)-> Void {
        guard let topVC = UIApplication.topTabbarViewController() else { return }
        let title = "กรุณาอนุญาติการเข้าถึงกล้อง"
        let goToSettingTxt = "ไปที่ตั้งค่า"
        let cancelText = "ยกเลิก"
        DialogController.share.showTwoBtnDialog(vc: topVC, title: title, message: "", okText: goToSettingTxt, onOK: {
            self.openAppSettings()
        }, cancelText: cancelText, onCancel: onCancel, canDissmisOK: true, canDissmisCancel: true)
    }
    
    func showDialogPhotoLibrarySetting(onCancel: (()->Void)? = nil) {
        let title = "กรุณาอนุญาตการเข้าถึงรูปภาพ"
        let goToSettingTxt = "ไปที่ตั้งค่า"
        let cancelText = "ยกเลิก"
        DispatchQueue.main.async {
            guard let topVC = UIApplication.topTabbarViewController() else { return }
            DialogController.share.showTwoBtnDialog(vc: topVC, title: title, message: "", okText: goToSettingTxt, onOK: {
                self.openAppSettings()
            }, cancelText: cancelText, onCancel: onCancel, canDissmisOK: true, canDissmisCancel: true)
        }
    }
    
    func checkPhotoPermission(onComplete: (()->Void)?, onCancel: (()->Void)? = nil) {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            onComplete?()
        case .denied, .restricted :
            showDialogPhotoLibrarySetting(onCancel: onCancel)
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    onComplete?()
                case .denied, .restricted:
                    self.showDialogPhotoLibrarySetting(onCancel: onCancel)
                case .notDetermined:
                    self.showDialogPhotoLibrarySetting(onCancel: onCancel)
                default:
                    break
                }
            }
        default:
            break
        }
    }
    
    func checkCameraPermission(onComplete: (()->Void)?, onCancel: (()->Void)? = nil) {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)

        switch cameraAuthorizationStatus {
        case .authorized:
            onComplete?()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                if response {
                    DispatchQueue.main.async {
                        onComplete?()
                    }
                } else {
                    DispatchQueue.main.async {
                        ManagerCore.share.showDialogCameraGoToSetting()
                    }
                }
            }
        default:
            ManagerCore.share.showDialogCameraGoToSetting()
        }
    }
    
}
