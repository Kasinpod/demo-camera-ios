//
//  CameraPreviewViewController.swift
//  fvu-ios-swift
//
//  Created by Admin on 5/7/2564 BE.
//  Copyright © 2564 BE Feyverly. All rights reserved.
//

import UIKit

final class CameraPreviewViewController: BaseViewController {
    
    // MARK: - Constants
    var listImage:[UIImage] = []
    
    // MARK: - IBOutlet
    @IBOutlet var imagesView: [UIImageView]!
    @IBOutlet var showView: UIView!
    
    // MARK: - Action
    @IBAction func closeAction(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    // MARK:- View lifecycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Function
    private func setupUI() {
        imagesView[0].image = downImge(cgImage: listImage[0].cgImage)
        imagesView[1].image = downImge(cgImage: listImage[1].cgImage)
        imagesView[2].image = downImge(cgImage: listImage[2].cgImage)
        imagesView[3].image = downImge(cgImage: listImage[3].cgImage)
        imagesView[4].image = downImge(cgImage: listImage[0].cgImage)
        imagesView[5].image = downImge(cgImage: listImage[1].cgImage)
        imagesView[6].image = downImge(cgImage: listImage[2].cgImage)
        imagesView[7].image = downImge(cgImage: listImage[3].cgImage)
    }
    
    private func downImge(cgImage: CGImage?) -> UIImage {
        return UIImage(cgImage: cgImage!, scale: 1.0, orientation: .down)
    }
    
    private func savePhoto() {
        guard let selectedImage = UIImage(snapshotOf: showView) else {
            print("Image not found!")
            return
        }
        UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showError(msg: "Save error\n\(error.localizedDescription)")
        } else {
            showError(msg: "Saved!\nYour image has been saved to your photos.")
        }
    }
    
    func blendImages(_ img: UIImage,_ imgTwo: UIImage) -> UIImage? {
        let bottomImage = img
        let topImage = imgTwo
        
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        let imgView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        
        // - Set Content mode to what you desire
        imgView.contentMode = .scaleAspectFill
        imgView2.contentMode = .scaleAspectFit
        
        // - Set Images
        imgView.image = bottomImage
        imgView2.image = topImage
        
        // - Create UIView
        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        contentView.addSubview(imgView)
        contentView.addSubview(imgView2)
        
        // - Set Size
        let size = CGSize(width: 300, height: 300)
        
        // - Where the magic happens
        UIGraphicsBeginImageContextWithOptions(size, true, 0)
        
        contentView.drawHierarchy(in: contentView.bounds, afterScreenUpdates: true)
        
        guard let i = UIGraphicsGetImageFromCurrentImageContext(),
              let data = i.jpegData(compressionQuality: 1.0)
        else {return nil}
        
        UIGraphicsEndImageContext()
        
        return UIImage(data: data)
    }

}
