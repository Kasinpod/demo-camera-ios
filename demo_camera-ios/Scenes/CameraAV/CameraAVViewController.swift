//
//  CameraAVViewController.swift
//  fvu-ios-swift
//
//  Created by Admin on 2/7/2564 BE.
//  Copyright © 2564 BE Feyverly. All rights reserved.
//

import UIKit
import CameraManager

final class CameraAVViewController: BaseViewController {
    
    // MARK: - Constants
    let cameraManager = CameraManager()
    var isFlashShow = false
    var listImage = [UIImage]()
    
    private lazy var listFlash: [UIView] = {
        [flashOnView, flashOffView, flashAutoView]
    }()
    
    private lazy var listImageView: [UIImageView] = {
        [imageOneView, imageTwoView, imageTreeView, imageFlowView]
    }()
    
    // MARK: - IBOutlet
    @IBOutlet var cameraView: UIView!
    
    @IBOutlet var imageOneView: UIImageView!
    @IBOutlet var imageTwoView: UIImageView!
    @IBOutlet var imageTreeView: UIImageView!
    @IBOutlet var imageFlowView: UIImageView!
    
    @IBOutlet var flashButton: UIButton!
    @IBOutlet var flashOnView: UIView!
    @IBOutlet var flashOffView: UIView!
    @IBOutlet var flashAutoView: UIView!
    
    @IBOutlet var flashOnLabel: UILabel!
    @IBOutlet var flashOffLabel: UILabel!
    @IBOutlet var flashAutoLabel: UILabel!
    
    // MARK:- View lifecycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCount()
        setupCameraManager()
        listFlash.forEach { $0.isHidden = true }
        ManagerCore.share.checkCameraPermission {
            self.addCameraToView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listImage.removeAll()
        setCount()
        cameraManager.resumeCaptureSession()
        cameraManager.startQRCodeDetection { result in
            switch result {
            case .success(let value):
                print(value)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopQRCodeDetection()
        cameraManager.stopCaptureSession()
    }
    
    // MARK: - Function
    private func setupCameraManager() {
        cameraManager.shouldEnableExposure = true
        cameraManager.writeFilesToPhoneLibrary = false
        cameraManager.shouldFlipFrontCameraImage = true
        cameraManager.showAccessPermissionPopupAutomatically = false
        cameraManager.flashMode = .off
        cameraManager.cameraDevice = .back
        cameraManager.cameraOutputMode = .stillImage
        cameraManager.cameraOutputQuality = .high
    }
    
    private func addCameraToView() {
        cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.stillImage)
        cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
            self?.showError(msg: "\(erTitle)\n\(erMessage)")
        }
    }
    
    private func setupFlashView() {
        listFlash.reversed().forEach { flashView in
            flashView.fadeOut()
        }
        isFlashShow = !isFlashShow
    }
    
    private func setCount() {
        if listImage.isEmpty {
            listImageView.forEach { $0.image = nil }
            return
        }
        for (i, item) in listImage.enumerated() {
            listImageView[i].image = item
        }
        
    }
    
    // MARK: - Action
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func swichtDevice(_ sender: UIButton) {
        if cameraManager.cameraDevice == .front {
            cameraManager.cameraDevice = .back
        } else if cameraManager.cameraDevice == .back {
            cameraManager.cameraDevice = .front
        }
    }
    
    @IBAction func selectFlash(_ sender: UIButton) {
        if isFlashShow {
            listFlash.reversed().forEach { flashView in
                flashView.fadeOut()
            }
        } else {
            listFlash.forEach { flashView in
                flashView.fadeIn()
            }
        }
        isFlashShow = !isFlashShow
    }
    
    @IBAction func flashOn(_ sender: UIButton) {
        cameraManager.flashMode = .on
        flashOnLabel.textColor = #colorLiteral(red: 1, green: 0.7568627451, blue: 0.02745098039, alpha: 1)
        flashOffLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        flashAutoLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        setupFlashView()
        flashButton.setImage(#imageLiteral(resourceName: "flash-on"), for: .normal)
    }
    
    @IBAction func flashOff(_ sender: UIButton) {
        cameraManager.flashMode = .off
        flashOnLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        flashOffLabel.textColor = #colorLiteral(red: 1, green: 0.7568627451, blue: 0.02745098039, alpha: 1)
        flashAutoLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        setupFlashView()
        flashButton.setImage(#imageLiteral(resourceName: "flash-off"), for: .normal)
    }
    
    @IBAction func flashAuto(_ sender: UIButton) {
        cameraManager.flashMode = .auto
        flashOnLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        flashOffLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        flashAutoLabel.textColor = #colorLiteral(red: 1, green: 0.7568627451, blue: 0.02745098039, alpha: 1)
        setupFlashView()
        flashButton.setImage(#imageLiteral(resourceName: "flash-auto"), for: .normal)
    }
    
    @IBAction func recordButtonTapped(_ sender: UIButton) {
        switch cameraManager.cameraOutputMode {
        case .stillImage:
            cameraManager.capturePictureWithCompletion { result in
                switch result {
                case .failure:
                    self.cameraManager.showErrorBlock("Error occurred", "Cannot save picture.")
                case .success(let content):
                    if let capturedData = content.asData {
                        let capturedImage = UIImage(data: capturedData)!
                        self.listImage.append(capturedImage)
                        self.setCount()
                        if self.listImage.count == 4 {
                            self.showIndicator()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.routToTermCameraPreview()
                            }
                            
                        }
                    }
                }
            }
        case .videoWithMic, .videoOnly:
            break
        }
    }
    
    // MARK:- Routing
    private func routToTermCameraPreview() {
        hideIndicator()
        let vc = StoryboardScene.CameraPreview.cameraPreviewViewController.instantiate()
        vc.listImage = listImage
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: false, completion: nil)
    }
    
}


