//
//  DialogView.swift
//  pricex-ios
//
//  Created by Admin on 22/12/2563 BE.
//  Copyright © 2563 BE Feyverly. All rights reserved.
//

import UIKit
import SwiftEntryKit

final class DialogView: UIView {
    
    @IBOutlet private var contentView: UIView?
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var okBtn: UIButton!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var spaceOkCancelView : UIView!
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    
    var onOK : (() -> Void)?
    var onCancel : (() -> Void)?
    var canDissmisOK = true
    var canDissmisCancel = true
    
    @IBAction func okWasPress(_ sender: UIButton) {
        onOK?()
        if canDissmisOK {
            dismissDiaog()
        }
    }
    
    @IBAction func cancelWasPress(_ sender: UIButton) {
        onCancel?()
        if canDissmisCancel {
            dismissDiaog()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("DialogView", owner: self, options: nil)
        guard let content = contentView else { return }
        self.addSubview(content)
        content.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            content.topAnchor.constraint(equalTo: topAnchor),
            content.leftAnchor.constraint(equalTo: leftAnchor),
            content.rightAnchor.constraint(equalTo: rightAnchor),
            content.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        
        widthConstraint.constant = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height) - 64
        roundCornerView(rad: 10)
    }
    
    func setupBtn(title: String,
                  btnOKText: String, onOK: (() -> Void)? = nil,
                  btnCancelText: String? = nil,
                  onCancel: (() -> Void)? = nil) -> Void {
        titleLbl.text = title
        okBtn.setTitle(btnOKText, for: .normal)
        cancelBtn.setTitle(btnCancelText, for: .normal)
        self.onOK = onOK
        self.onCancel = onCancel
        if btnCancelText == nil {
            spaceOkCancelView.isHidden = true
            cancelBtn.isHidden = true
        }
    }
    
    func dismissDiaog() -> Void {
        SwiftEntryKit.dismiss()
    }
}
