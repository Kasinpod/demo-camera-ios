//
//  BaseViewController.swift
//  
//
//  Created by Admin on 22/10/2562 BE.
//  Copyright © 2562 Feyverly. All rights reserved.
//

import UIKit

class BaseViewController : UIViewController {
    
    var topSafeArea: CGFloat = 0
    var bottomSafeArea: CGFloat = 0
    private var bottomConstraint: NSLayoutConstraint?
    private var margin: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.barStyle = .black
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        topSafeArea = view.safeAreaInsets.top
        bottomSafeArea = view.safeAreaInsets.bottom
    }
    
    func showError(msg: String, onOK: (() -> Void)? = nil) {
        hideIndicator()
        showDialog(msg: msg, onOK: onOK)
    }
    
    func registerNotificationsKeyboard(bottomConstraint: NSLayoutConstraint? = nil, margin: CGFloat = 0) {
        self.bottomConstraint = bottomConstraint
        self.margin = margin
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeNotificationsKeyboard() {
        self.bottomConstraint = nil
        self.margin = 0
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardNotification(_ notification: Notification) {
        if let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let h = keyboardFrame.height - bottomSafeArea - margin
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            bottomConstraint?.constant = isKeyboardShowing ? h : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
}

extension BaseViewController : UIGestureRecognizerDelegate {
    
}
